Crop Volume
-----------

Brief Description
^^^^^^^^^^^^^^^^^

This operation lets the user to select a rectangular volume of interest. It takes as input a volume VME selected from the tree (rectilinear grid or structured point).

Usage
^^^^^

The user can select the region of interest interactively by using the gizmos/handles available in the current view or by using the textual entries.

Interactive mode: With the mouse the user can move the ROI boundaries after selecting the corresponding face handle (the handle become yellow when selected). Both the handles and the ROI boundaries can be taken off the visualisation by un-checking the corresponding boxes.

Text entries: The user can insert the new boundary limits in each of the coordinate system directions buy updating the corresponding text boxes. Values outside the current volume limits will not be accepted.

In both modalities, the cropping will be approximated to the closest voxel dimensions (no cut or interpolations are performed to the scalar values).

The ROI can be reset at any time with the available button. When the OK button is pressed the volume will be updated (or a new one will be generated) and the visualisation automatically updated to the new ROI.

Test Datasets
^^^^^^^^^^^^^

They can be downloaded from `here<https://www.dropbox.com/sh/py9qnlyaeaugfd2/AABYEoPkoQqPNdFonVrm0INVa>`_. 
