Create Volume
-------------

Brief Description
^^^^^^^^^^^^^^^^^

The function allows to create a volume with rectangular cross section
by defining the spacing along each direction, the dimension along each
direction, and the scalar value that density in the space.

Usage
^^^^^

* Call function: “Operation/Create/New/Constant Volume”
* Add a view to visualize the volume: “View/Add View/…”
* Select parent VME
* In the “Control Bar/operation” there is the control table to define
  the dimension and density of the new volume to create:

  - In the fields “xyz spc” enter the spacing along x, y and z
  - In the fields “xyz dim” enter the dimensions of the volume along
    x, y and z
  - In the field “density” enter the constant scalar value that will
    be assigned to the generated volume *The VME will be added in the tree
    according to the selected view *Tick the circle/square next to the VME
    to show the created volume.

Test Datasets
^^^^^^^^^^^^^

They can be downloaded from
`here<https://www.dropbox.com/sh/pqsdk73tcs3979e/AAB14FqrvsnPsbfU6W9m6H6qa>`_.
