Bonemat
-------

Brief Description
^^^^^^^^^^^^^^^^^

This function is used to calculate the Young’s modulus values of a
given finite element (bone) mesh from the CT scans.

Usage
^^^^^

Standard:

* File -> import -> Other -> VTK -> PM4.vtk
* File -> Import -> Finite Element -> RFemur.cdb
* Select the mesh on RHS panel -> Operations -> Modify -> Move, (RHS
  panel) Operation -> interaction modality = gizmo -> Gizmo rotate ->
  Rotate Z = 180 -> ok (Note this step only applies to this specific
  mesh as it is not aligned with the CT scans)
* Select the target mesh -> Operations -> Modify -> Bonemat -> accept
  request to make a copy of the mesh
* Under operation tab -> Open config file -> Calibration, Choose
  volume -> PM CT image, Execute -> ok, (when completed) -> ok (Please
  see section below for details of calibration parameters)
* Select the mesh -> export -> Finite Element -> Ansys input file

This will generate an output file with user specified name, an example
output file called Results.inp is included in the folder.

To save a particular set of parameters for future use, click “Save
config file”

Test Datasets
^^^^^^^^^^^^^

They can be downloaded from `here<https://www.dropbox.com/sh/v0kpnsukmunowjt/AAAFi3ceSrhYr6HxwIxkI9oqa>`_.
