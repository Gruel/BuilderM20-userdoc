Add Landmark
------------

Brief Description
^^^^^^^^^^^^^^^^^

Allows addition of a landmark (VMElandmark) onto the parent object.

Usage
^^^^^

This operation creates a cloud of VMELandmark objects. The cloud is a
child of the VME selected when the operation is launched. Select the
desired parent body and the select the add landmark operation.

There are two options to define the name of the landmark:

* A dictionary of landmark names can be used. Click the “load
  dictionary” button and locate a “.dic” file (an example of a
  dictionary file is the file called “dictionary.dic” located in the
  subfolder NMSBuilder\Config\Dictionary. In this file the anatomical
  landmarks of the femur; described in Kepple TM et al., "A
  three-dimensional musculoskeletal database for the lower
  extremities", J Biomech. 1998 Jan;31(1):77-80; can be found. Select
  a name from the loaded list (above the operation controls).

* Type the desired landmark name in the “landmark name” text box.

There are then two options to define the position of the landmark:
click on the desired location on the parent VME or alternatively type
the coordinates of the landmark into the x, y and z “position” text
boxes and then click “add landmark”.

Repeat process until all landmarks are added and then click “OK”, or
pressing “cancel” will end the operation without adding any of the
landmarks.

Once the landmark cloud is created, the user can add other landmarks
by performing the “Add landmark” operation with the target landmark
cloud VME selected. To perform this action the landmark cloud has to
be exploded. The user can change the cloud status using the “Explode”
checkbox in the VME tab. The radius and the resolution of the
landmarks can also be changed in the VME tab.

An example of format of a dictionary (.dic) file::

  <?xml version="1.0" encoding="ISO-8859-1"?>
  <DIC Name="database_name" Version="2.0">
      <Dictionary>
         <DItem>example_landmark_name1</DItem>
         <DItem> example_landmark_name2</DItem>
         </Dictionary>
  </DIC>

Test Datasets
^^^^^^^^^^^^^

They can be downloaded from
`here<https://www.dropbox.com/sh/a7jr6loibx3f63i/AABygBJeQOM3KBQZGOdHxl6ia>`_.

Tips
^^^^


* The tab button must be pressed after the name is entered!

* When clicking the “mafVMELandmark” button it would be nice to have
  the transformation (rotation and translation) matrix in both the
  global (root) and the parent coordinate frames, rather than only the
  global (the current case).
