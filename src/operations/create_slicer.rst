Create Slicer
-------------

Function
^^^^^^^^

* Operation>create>derive>Slicer

General Description
^^^^^^^^^^^^^^^^^^^

* select Operation>create>derive>Slicer and then select the volume to
  slice(The volume must be already loaded in the data tree)
* The function creates a Slicer that is a section of the selected
  volume.
* The default position is a section normal to the z-axis in the middle
  of the bounding box of the volume.
* The created VME is a surface, child of the volume, and it's
  displayed with the default grey LUT.
* As any other VME the slicer can be moved obtaining an arbitrary
  slice of the specified volume.

Usage
^^^^^

* Call the function 'Operation>create>derive>Slicer' and select the
  volume to slice; the default section is normal to the z-axis in the
  middle of the bounding box of the volume.
* Move the Slicer to find the desired section of the volume.
* You can change the attributes of the slicer in the 'visual
  props'(only in the surface view) and 'vme' panels (including the
  volume to section)
* Open a view and check the slicer created.

Test
^^^^

Data
""""

Volume_test1_rect.vtk a cubic vtk rectilinear grid volume (edge
length 100) density 100 with 12 bubbles inside. The bubbles are
organized in 3 series (on different YX planes) of 4 bubbles. Every
series is composed by 4 bubbles with different diameter (20, 10, 5, 1)
but with the same density. The series have different density that are
90,105 and 106.

* Volume_test1_struct.vtk

the same volume as 'Volume_test1_rect.vtk' but saved as structured
points.

* Zsn.stl

spherical surfaces of the bubbles as described in
Volume_test1_rect.vtk where 's' is the number of the series and 'n' is
the number of the sphere.

* test_Slicer.msf

msf file with all the data used to check the operation.

* Slicer1_rect_paraview.vtk

slice obtained from the rectilinear grid volume
(Volume_test1_rect.vtk) in Paraview

* Slicer1_struct_paraview.vtk

slice obtained from the structured points volume
(Volume_test1_struct.vtk)in Paraview

Two slicers at arbitrary position were created on Volume_test1_rect
and Volume_test1_struct; the same slices were done on ParaView(version
3.14.1) and saved as VTK. The slices obtained by the two software were
compared and the superimposition with the surfaces of the bubbles was
checked. For each slicer the values of density of the volume and the
bubbles were checked using different LUTs. The function passed the
test with just a note.

.. note::

   Obviously cutting a volume in an arbitrary position an
   interpolation is necessary but I saw that the interpolation affects
   even uniform parts changing the value in the second decimal
   position. Maybe this is due to the interpolation precision.
