Create Parametric Surface
-------------------------

Function
^^^^^^^^

* Operation>create>new>Parametric Surface

General Description
^^^^^^^^^^^^^^^^^^^

This function creates a parametric surface Surfaces available :

* Sphere
* Cone
* Cylinder
* Cube
* Plane
* Ellipsoid
* Dimensions, orientation and resolution can be specified inside the
  operation typing the numeric values or selecting checkboxes

Usage
^^^^^

* Call the function 'Operation>create>new>Parametric Surface' a
  default sphere is created.
* Choose in the 'vme' panel from the drop down list the desired type
  of surface.
* Write in the related part of the vme panel the dimentions,
  orientation and resolution.
* Open a view and check the surface created.

Test
^^^^


* Creation of all type of surfaces and check in the surface view
  assigning "wireframe material" in the visual props.
* The data are in the file Test_Create_parametric_surface.msf
* Export the surfaces as 'stl', open them with another software and
  check the surfaces again.
* The data are in the folder Create_parametric_surface and in the file
  names are specified the ttpe of surface and the parameters in the
  same order as in the vme panel.
