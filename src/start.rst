#############
Getting Start
#############

Installation
============

You can get the latest version of BuilderM2O from `Sourceforge <https://sourceforge.net/projects/builderm2o/files/latest`_. 
By double clicking the exe file, the installation process will be 
started by the wizard. Please follow the Next button to complete it. 

