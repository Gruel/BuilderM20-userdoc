RXCT
----

This view allows the visualization of the anterior/posterior view and
the Medial/Lateral view of a DICOM file, illustrated by two big
panels. The other six panels show the axial view of the images and the
slices can be moved using the six gizmos.
