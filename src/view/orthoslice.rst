OrthoSlice
----------

Here the process uses dicom images as an example.

1. import dicom images by 'File' > 'Import' -> 'Images'> 'dicom';

2. in the pop out window (dicom Importer), click
   'crop'->'build'->'finish'. The image dataset will be loaded in
   BuilderM2O;

3. attach OrthoSlice to the image dataset by 'View'-> 'Add View' ->
   'OrthoSlice';

4. tick the circle next to the database to view the images in
   different views. you can move the planes to change the view
   locations. 
