#####################
How to use BuilderM2O
#####################


Menu File
=========

.. toctree::
   :maxdepth: 2

   Importers <file/importers.rst>
   Exporters <file/exporters.rst>

Menu View
=========

.. toctree::
   :maxdepth: 2

   RXCT <view/rxct.rst>
   OrthoSlice <view/orthoslice.rst>
   
Menu Operations
===============

.. toctree::
   :maxdepth: 2

    Add Landmark <operations/add_landmark.rst>
    Create Volume <operations/create_volume.rst>
    Crop Volume <operations/crop_volume.rst>
    Bonemat <operations/bonemat.rst>
    Create Parametric Surface <operations/create_parametric_surface.rst>
    Create Slicer <operations/create_slicer.rst>

    
