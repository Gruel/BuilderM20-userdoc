Exporters
---------

VTK
^^^

This expoters saves the VME information into VTK file format. The
exporter is active only for those VME types which supports the VTK
format (voumes, surfaces, polylines, meshes). The user is asked first
to select a name and a folder for the destination file. Then, he/she
can choose if the VTK file will be stored in ASCII or binary format
and, in case a position matrix different from the global system is
available in the VME, if this position has to be applied to the saved
VTK data. UNSIGNED SHORT scalar format can also be forced before
saving.
