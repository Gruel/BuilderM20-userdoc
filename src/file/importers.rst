Importers
---------

VTK
^^^

The VTK importer loads data stored in vtk file format (both ASCII and
binary coding are supported). The type of VME created into the data
tree depends on the information contained into the vtk file.
