##########
mafVMERoot
##########

The MafVMERoot is a special type of Virtual Medical Entity (VME). It
is created automatically any time a new VME Tree is created (Menu
File: New). It is an empty VME, whose pose matrix is the identity
matrix, and that is the parent of any other VME that is created. Its
default name is “Root” although it can be changed in the tab “vme”. To
it is also associated the metadata ApplicationStamp, that defines
which application created the VmeTree (Current setting is
BuilderM2O). The figure on the side shows the VmeTree view, normally
on the right side of the screen, with the vme tab selected, and the
name of the VmeRoot changed to “Frank”.

.. image:: ../images/MafRootVME.png
   :align: center
