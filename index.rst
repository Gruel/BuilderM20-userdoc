.. openmaf documentation master file. You can adapt this file
   completely to your liking, but it should at least contain the root
   `toctree` directive.

.. |latex| replace:: :math:`LaTeX`

##########################
OpenMAF User Documentation
##########################

.. sidebar:: Summary

    :Name: **project**
    :Release: |release|
    :version: |version| 
    :Date: |today|
    :Authors: 
    :Target: developers
    :status: draft

.. topic:: OpenMAF Document Center

    * OpenMAF official documents
    * BuilderM2O

.. toctree::
   :maxdepth: 2

   approvals
   changes
   scope
   acronyms
   Getting Start <src/start.rst>
   How-To use BuilderM2O <src/how-to.rst>
   VMEs <src/VMEs.rst>


..	Modules/index

.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

