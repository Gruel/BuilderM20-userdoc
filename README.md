BuilderM20-userdoc
==================

Enduser documentation for BuilderM2O

To contribute to this doc or to compile it:

Download the template with the submodules (needed to use readthedoc theme):

- git clone https://github.com/gruel/BuilderM20-userdoc.git --recursive
